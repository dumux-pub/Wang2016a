// $Id: 2p2cfluxvariables.hh 5151 2011-02-01 14:22:03Z lauser $
/*****************************************************************************
 *   Copyright (C) 2008-2009 by Klaus Mosthaf                                *
 *   Copyright (C) 2008-2009 by Andreas Lauser                               *
 *   Copyright (C) 2008 by Bernd Flemisch                                    *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief   This file contains the data which is required to calculate
 *          all fluxes of components over a face of a finite volume for
 *          the two-phase biomineralisation model.
 */
/*!
 * \ingroup TwoPNCMinModel
 */
#ifndef DUMUX_2PBIOMIN_FLUX_VARIABLES_HH
#define DUMUX_2PBIOMIN_FLUX_VARIABLES_HH

#include <dumux/common/math.hh>
#include <dumux/common/spline.hh>
#include "dumux/porousmediumflow/2pncmin/implicit/fluxvariables.hh"

namespace Dumux
{

/*!
 * \brief This template class contains the data which is required to
 *        calculate all fluxes of components over a face of a finite
 *        volume for the two-phase biomineralisation model.
 *
 * This means pressure and concentration gradients, phase densities at
 * the integration point, etc.
 */
template <class TypeTag>
class TwoPBioMinFluxVariables : public GET_PROP_TYPE(TypeTag, BaseFluxVariables)
{
    typedef typename GET_PROP_TYPE(TypeTag, BaseFluxVariables) BaseFluxVariables;
    typedef TwoPNCMinFluxVariables<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),
    };

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename FVElementGeometry::SubControlVolume SCV;
    typedef typename FVElementGeometry::SubControlVolumeFace SCVFace;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef Dune::FieldVector<CoordScalar, dimWorld> DimVector;
    typedef Dune::FieldMatrix<CoordScalar, dim, dim> DimMatrix;

    enum {
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,

        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
    };

public:
    /*!
     * \brief Compute / update the flux variables
     *
     * \param problem The problem
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param fIdx The local index of the SCV (sub-control-volume) face
     * \param elemVolVars The volume variables of the current element
     * \param onBoundary A boolean variable to specify whether the flux variables
     * are calculated for interior SCV faces or boundary faces, default=false
     */
    void update(const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int fIdx,
                const ElementVolumeVariables &elemVolVars,
                const bool onBoundary = false)
    {
        BaseFluxVariables::update(problem, element, fvGeometry, fIdx, elemVolVars, onBoundary);

        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx) {
            density_[phaseIdx] = Scalar(0);
            molarDensity_[phaseIdx] = Scalar(0);
            potentialGrad_[phaseIdx] = Scalar(0);
            pressureGrad_[phaseIdx] = Scalar(0);
            for (int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
                massFractionGrad_[phaseIdx][compIdx] = Scalar(0);
                moleFractionGrad_[phaseIdx][compIdx] = Scalar(0);
            }
        }
        calculateGradients_(problem, element, elemVolVars);
        calculateK_(problem, element, elemVolVars);
        calculateVelocities_(problem, element, elemVolVars);
        calculateporousDiffCoeff_(problem, element, elemVolVars);
        calculateporousDispersionTensor_(problem, element, elemVolVars);
    }

    void calculateGradients_(const Problem &problem,
                             const Element &element,
                             const ElementVolumeVariables &elemVolVars)
    {
        // calculate gradients
        DimVector tmp(0.0);
        for (int idx = 0;
             idx < this->fvGeometry_().numScv;
             idx++) // loop over adjacent vertices
        {
            // FE gradient at vertex idx
            const DimVector &feGrad = face().grad[idx];

            // compute sum of pressure gradients for each phase
            for (int phaseIdx = 0; phaseIdx < numPhases; phaseIdx++)
            {
                // the pressure gradient
                tmp = feGrad;
                tmp *= elemVolVars[idx].pressure(phaseIdx); //Phasen Druck
                potentialGrad_[phaseIdx] += tmp;
                pressureGrad_[phaseIdx] += tmp;
            }

            // the concentration gradient of the non-wetting
            // component in the wetting phase

            for(int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
            {
                for(int compIdx = 0; compIdx < numComponents; ++compIdx)
                {
                    if(compIdx != phaseIdx) //No grad is needed for this case
                    {
                        tmp = feGrad;
                        tmp *= elemVolVars[idx].massFraction(phaseIdx, compIdx);
                        massFractionGrad_[phaseIdx][compIdx] += tmp;

                        tmp = feGrad;
                        tmp *= elemVolVars[idx].moleFraction(phaseIdx, compIdx);
                        moleFractionGrad_[phaseIdx][compIdx] += tmp;
                    }
                }
            }
        }

        // correct the pressure gradients by the hydrostatic
        // pressure due to gravity
        for (int phaseIdx=0; phaseIdx < numPhases; phaseIdx++)
        {
            int i = face().i;
            int j = face().j;
            Scalar fI = rhoFactor_(phaseIdx, i, elemVolVars);
            Scalar fJ = rhoFactor_(phaseIdx, j, elemVolVars);
            if (fI + fJ <= 0)
                fI = fJ = 0.5; // doesn't matter because no phase is
                               // present in both cells!
            density_[phaseIdx] =
                (fI*elemVolVars[i].density(phaseIdx) +
                 fJ*elemVolVars[j].density(phaseIdx))
                /
                (fI + fJ);
            // phase density
            molarDensity_[phaseIdx]
                =
                (fI*elemVolVars[i].molarDensity(phaseIdx) +
                 fJ*elemVolVars[j].molarDensity(phaseIdx))
                /
                (fI + fJ); //arithmetic averaging

            tmp = problem.gravity();
            tmp *= density_[phaseIdx];

            potentialGrad_[phaseIdx] -= tmp;
        }
    }

    Scalar rhoFactor_(int phaseIdx, int scvIdx, const ElementVolumeVariables &vDat)
    {

        static const Scalar eps = 1e-2;
        const Scalar sat = vDat[scvIdx].density(phaseIdx);
        if (sat > eps)
            return 0.5;
        if (sat <= 0)
            return 0;

        static const Dumux::Spline<Scalar> sp(0, eps, // x0, x1
                                              0, 0.5, // y0, y1
                                              0, 0); // m0, m1
        return sp.eval(sat);
    }

    void calculateK_(const Problem &problem,
            const Element &element,
            const ElementVolumeVariables &elemVolVars)
    {
            const SpatialParams &spatialParams = problem.spatialParams();
            const VolumeVariables &volVarsI = elemVolVars[face().i];
            const VolumeVariables &volVarsJ = elemVolVars[face().j];

            DimMatrix K_i(0.0);
            DimMatrix K_j(0.0);
            K_i = spatialParams.intrinsicPermeability(element,this->fvGeometry_(),face().i);
            K_j = spatialParams.intrinsicPermeability(element,this->fvGeometry_(),face().j);

            K_i *= volVarsI.permeabilityFactor();
            K_j *= volVarsJ.permeabilityFactor();
//            for(int i=0; i<dim; i++)
//            {
//              K_i[i][i] *= volVarsI.permeabilityFactor();
//              K_j[i][i] *= volVarsJ.permeabilityFactor();
//            }
           // std::cout<<"Perm : "<<K_j[0][0]<<std::endl;
            spatialParams.meanK(K_,K_i,K_j);


    }

    void calculateVelocities_(const Problem &problem,
                              const Element &element,
                              const ElementVolumeVariables &elemVolVars)
    {
       // const SpatialParams &spatialParams = problem.spatialParams();
        // multiply the pressure potential with the intrinsic
        // permeability
        DimMatrix K = K_;
        for (int phaseIdx=0; phaseIdx < numPhases; phaseIdx++)
        {
            K.mv(potentialGrad_[phaseIdx], Kmvp_[phaseIdx]);
            KmvpNormal_[phaseIdx] = - (Kmvp_[phaseIdx] * face().normal);
        }

        // set the upstream and downstream vertices
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            upstreamIdx_[phaseIdx] = face().i;
            downstreamIdx_[phaseIdx] = face().j;

            if (KmvpNormal_[phaseIdx] < 0) {
                std::swap(upstreamIdx_[phaseIdx],
                          downstreamIdx_[phaseIdx]);
            }
        }
    }

    void calculateporousDiffCoeff_(const Problem &problem,
                                   const Element &element,
                                   const ElementVolumeVariables &elemVolVars)
    {
        const VolumeVariables &volVarsI = elemVolVars[face().i];
        const VolumeVariables &volVarsJ = elemVolVars[face().j];

        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            /* If there is no phase saturation on either side of the face
             * no diffusion takes place */

            if (volVarsI.saturation(phaseIdx) <= 0 ||
                volVarsJ.saturation(phaseIdx) <= 0)
               {
                for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                    {
                      porousDiffCoeff_[phaseIdx][compIdx] = 0.0;
                    }
               }

            else
            {
            // calculate tortuosity at the nodes i and j needed
            // for porous media diffusion coefficient
            Scalar tauI =  1.0/(volVarsI.porosity() * volVarsI.porosity()) *
                            pow(volVarsI.porosity() * volVarsI.saturation(phaseIdx), 7.0/3);

            Scalar tauJ =   1.0/(volVarsJ.porosity() * volVarsJ.porosity()) *
                            pow(volVarsJ.porosity() * volVarsJ.saturation(phaseIdx), 7.0/3);
            // Diffusion coefficient in the porous medium

            // -> harmonic mean
            for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                {
                    if(phaseIdx==compIdx)
                        porousDiffCoeff_[phaseIdx][compIdx] = 0.0;
                    else
                    {
                        porousDiffCoeff_[phaseIdx][compIdx] = harmonicMean(volVarsI.porosity() * volVarsI.saturation(phaseIdx) * tauI * volVarsI.diffCoeff(phaseIdx, compIdx),
                                                                           volVarsJ.porosity() * volVarsJ.saturation(phaseIdx) * tauJ * volVarsJ.diffCoeff(phaseIdx, compIdx));
                    }
                }
            }
        }
    }

    /*!
    * \brief Calculation of the dispersion
    *
    *        \param problem The considered problem file
    *        \param element The considered element of the grid
    *        \param elemDat The parameters stored in the considered element
    */
    void calculateporousDispersionTensor_(const Problem &problem,
                                    const Element &element,
                                    const ElementVolumeVariables &elemDat)
    {
        const GlobalPosition pos = element.geometry().center();

        const VolumeVariables &vDat_i = elemDat[face().i];
        const VolumeVariables &vDat_j = elemDat[face().j];

        //calculate dispersivity at the interface: [0]: alphaL = longitudinal disp. [m], [1] alphaT = transverse disp. [m]
        Scalar dispersivity[2];
//        dispersivity[0] = 0.5 * (vDat_i.dispersivity()[0] +  vDat_j.dispersivity()[0]);
//        dispersivity[1] = 0.5 * (vDat_i.dispersivity()[1] +  vDat_j.dispersivity()[1]);
        dispersivity[0] = alphaL_;//0.025; //alphaL [m]
        dispersivity[1] = alphaL_/10; //alphaT [m]

        //calculate velocity  v = 1/phi * vDarcy = -1/phi *mobility* K * grad(p)
        DimVector velocity[numPhases];
        Scalar vNorm[numPhases];
        Scalar S[numPhases];
        Scalar porosity = 0.5 * (vDat_i.porosity()  + vDat_j.porosity());
//        Valgrind::CheckDefined(potentialGrad());
//        Valgrind::CheckDefined(K_);

        for (int phaseIdx=0; phaseIdx < numPhases; phaseIdx++)
        {
            K_.mv(potentialGrad_[phaseIdx], velocity[phaseIdx]);
            velocity[phaseIdx] *= 0.5 * (vDat_i.mobility(phaseIdx)  + vDat_j.mobility(phaseIdx));

//          velocity[phaseIdx][0] = 0.9*3.2892e-4;
//          velocity[phaseIdx][1] = 0;

            S[phaseIdx] = 0.5 * (vDat_i.saturation(phaseIdx)  + vDat_j.saturation(phaseIdx));

            velocity[phaseIdx] /= - porosity;

            //normalized velocity
            vNorm[phaseIdx] = 0;
//              vNorm[phaseIdx] = velocity[phaseIdx].two_norm();
            for (int i=0; i<dim; i++)
            {
                vNorm[phaseIdx] +=  velocity[phaseIdx][i]*velocity[phaseIdx][i];
            }
            vNorm[phaseIdx] = sqrt(vNorm[phaseIdx]);

//          if(phaseIdx==wPhaseIdx)
//          std::cout         << "vNorm = "; std::cout.width(5); std::cout<< vNorm[phaseIdx]<< ";    velocity = "; std::cout.width(5); std::cout<< velocity[phaseIdx] <<std::endl;

            for (int compIdx=0; compIdx < numComponents; compIdx++)
            {
                //matrix multiplication of the velocity at the interface: vv^T
                dispersionTensor_[phaseIdx][compIdx] = 0;
                for (int i=0; i<dim; i++)
                for (int j = 0; j<dim; j++)
                    dispersionTensor_[phaseIdx][compIdx][i][j] = velocity[phaseIdx][i]*velocity[phaseIdx][j];

//              if(phaseIdx==wPhaseIdx && compIdx==nCompIdx)
//              std::cout         << "dispTensor_ vv^T = "; std::cout.width(5); std::cout<< dispersionTensor_[phaseIdx][compIdx] <<std::endl;

                //normalize velocity product --> vv^T/||v||, [m/s]
                dispersionTensor_[phaseIdx][compIdx] /= vNorm[phaseIdx];
                if (vNorm[phaseIdx] < 1e-20)
                    dispersionTensor_[phaseIdx][compIdx] = 0;

//              if(phaseIdx==wPhaseIdx && compIdx==nCompIdx)
//              std::cout         << "dispTensor_ vv^T/||v|| = "; std::cout.width(5); std::cout<< dispersionTensor_[phaseIdx][compIdx] <<std::endl;

                //multiply with dispersivity difference: vv^T/||v||*(alphaL - alphaT), [m^2/s] --> alphaL = longitudinal disp., alphaT = transv. disp.
                dispersionTensor_[phaseIdx][compIdx] *= (dispersivity[0] - dispersivity[1]);

//              if(phaseIdx==wPhaseIdx && compIdx==nCompIdx)
//              std::cout         << "dispTensor_ vv^T/||v||*(alphaL - alphaT) = "; std::cout.width(5); std::cout<< dispersionTensor_[phaseIdx][compIdx] <<std::endl;

                //add ||v||*alphaT to the main diagonal:vv^T/||v||*(alphaL - alphaT) + ||v||*alphaT, [m^2/s]
                for (int i = 0; i<dim; i++)
                {
                    dispersionTensor_[phaseIdx][compIdx][i][i] += vNorm[phaseIdx]*dispersivity[1];
//                  if(phaseIdx==wPhaseIdx && compIdx==nCompIdx)
//                  std::cout         << "dispTensor_ vv^T/||v||*(alphaL - alphaT) + ||v||*alphaT = "; std::cout.width(5); std::cout<< dispersionTensor_[phaseIdx][compIdx] <<std::endl;

                    //multiply all entries of the Tensor by Saturation and porosity
                    for (int j = 0; j<dim; j++)
                    {
                        dispersionTensor_[phaseIdx][compIdx][i][j] *= S[phaseIdx]*porosity;
                    }
                    //add the porous diffusion coefficient
                    dispersionTensor_[phaseIdx][compIdx][i][i] += porousDiffCoeff_[phaseIdx][compIdx];
                }
//                      if(compIdx==0 && phaseIdx==0)
//                      std::cout         <<"position: "<<pos[0]<<", D [phaseIdx=0][compIdx=0][0][0] = "<< dispersionTensor_[phaseIdx][compIdx][0][0]<< ", velocity = "<< velocity[phaseIdx][0] <<std::endl;

                dispersionTensor_[phaseIdx][compIdx].mv(face().normal,normalDisp_[phaseIdx][compIdx]);
            }
        }

#ifdef DUMUX_HPCOREPROBLEM_HH
        if (pos[0] > 0.0508)        // for the hp core experiment only, to avoid "drainage" during no flow periods from the dirichlet=0 boundary
        {
            for(int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
            {
                for(int compIdx = 0; compIdx < numComponents; ++compIdx)
                {
                    normalDisp_[phaseIdx][compIdx]=0;
                }
            }
        }
#endif

    }

public:
    /*!
     * \copydoc 2pncMin::KmvpNormal
     */
    Scalar KmvpNormal(int phaseIdx) const
    { return KmvpNormal_[phaseIdx]; }

    /*!
     * \copydoc 2pncMin::Kmvp
     */
    DimVector Kmvp(int phaseIdx) const
    { return Kmvp_[phaseIdx]; }

    /*!
     * \copydoc 2pncMin::upstreamIdx
     */
    int upstreamIdx(int phaseIdx) const
    { return upstreamIdx_[phaseIdx]; }

    /*!
     * \copydoc 2pncMin::downstreamIdx
     */
    int downstreamIdx(int phaseIdx) const
    { return downstreamIdx_[phaseIdx]; }

    /*!
     * \copydoc 2pncMin::porousDiffCoeff
     */
    Scalar porousDiffCoeff(int phaseIdx, int compIdx) const
    { return porousDiffCoeff_[phaseIdx][compIdx];}

    /*!
     * \brief The binary dispersion Tensor for each fluid phase and component.
     */
    DimMatrix Dispersion(int phaseIdx, int compIdx) const
    { return dispersionTensor_[phaseIdx][compIdx]; };

        /*!
     * \brief The absolute pressure gradient for each fluid phase.
     */
    Scalar absgradp(int phaseIdx) const
    {
     Scalar absgradp_ = 0;
     for (int i=0;i<dim;i++)
     {
         absgradp_ += potentialGrad_[phaseIdx][i] * potentialGrad_[phaseIdx][i];
     }
     absgradp_ = sqrt(absgradp_);
     return absgradp_;
    }

    /*!
     * \copydoc 2pncMin::density
     */
    Scalar density(int phaseIdx) const
    { return density_[phaseIdx]; }

    /*!
     * \copydoc 2pncMin::molarDensity
     */
    Scalar molarDensity(int phaseIdx) const
    { return molarDensity_[phaseIdx]; }

    /*!
     * \copydoc 2pncMin::massFractionGrad
     */
    const DimVector &massFractionGrad(int phaseIdx, int compIdx) const
    { return massFractionGrad_[phaseIdx][compIdx]; }

    /*!
     * \copydoc 2pncMin::moleFractionGrad
     */
    const DimVector &moleFractionGrad(int phaseIdx, int compIdx) const
    { return moleFractionGrad_[phaseIdx][compIdx]; }

    const SCVFace &face() const
    {
    if (this->onBoundary_)
        return this->fvGeometry_().boundaryFace[this->faceIdx_];
    else
        return this->fvGeometry_().subContVolFace[this->faceIdx_];
    }
    const DimVector &normalDisp(int phaseIdx, int compIdx) const
    {
        return normalDisp_[phaseIdx][compIdx];
    }


protected:
//    const FVElementGeometry &fvGeometry_;
//    const int faceIdx_;
//    const bool onBoundary_;

    // gradients
    DimVector pressureGrad_[numPhases];
    DimVector potentialGrad_[numPhases];
    DimVector massFractionGrad_[numPhases][numComponents];
    DimVector moleFractionGrad_[numPhases][numComponents];

    // density of each face at the integration point
    Scalar density_[numPhases], molarDensity_[numPhases];

    // intrinsic permeability times pressure potential gradient
    DimVector Kmvp_[numPhases];
    // projected on the face normal
    Scalar KmvpNormal_[numPhases];

    // local index of the upwind vertex for each phase
    int upstreamIdx_[numPhases];
    // local index of the downwind vertex for each phase
    int downstreamIdx_[numPhases];

    DimMatrix K_;

    // the diffusion coefficient for the porous medium
    Dune::FieldMatrix<Scalar, numPhases, numComponents> porousDiffCoeff_;

    //! the dispersion tensor in the porous medium
    Dune::FieldMatrix<DimMatrix, numPhases, numComponents> dispersionTensor_;
//    DimMatrix dispersionTensor_[numPhases][numComponents];
    Dune::FieldMatrix<DimVector, numPhases, numComponents> normalDisp_;
//    DimVector normalDisp_[numPhases][numComponents];
//    Scalar Dispersion_; //just for output
    //the longitudinal dispersion length
    Scalar alphaL_ = 0.025; //alphaL [m] //TODO
};

} // end namespace

#endif
