// $Id: 2p2clocalresidual.hh 3795 2010-06-25 16:08:04Z melanie $
/*****************************************************************************
 *   Copyright (C) 2008-2009 by Klaus Mosthaf                                *
 *   Copyright (C) 2008-2009 by Bernd Flemisch                               *
 *   Copyright (C) 2009-2010 by Andreas Lauser                               *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the two-phase biomineralisation box model.
 */

#ifndef DUMUX_2PTHERMALMIN_LOCAL_RESIDUAL_BASE_HH
#define DUMUX_2PTHERMALMIN_LOCAL_RESIDUAL_BASE_HH

#include <dumux/common/math.hh>

#include "dumux/porousmediumflow/2pncmin/implicit/properties.hh"
#include "dumux/porousmediumflow/2pncmin/implicit/indices.hh"

#include "dumux/implicit/2pbiomin/2pbiominfluxvariables.hh"
#include "dumux/porousmediumflow/2pncmin/implicit/localresidual.hh"

#include <iostream>
#include <vector>

//#define VELOCITY_OUTPUT 1 // uncomment this line if an output of the velocity is needed

namespace Dumux
{
/*!
 * \ingroup TwoPNCMinModel
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the two-phase n-component mineralisation box model.
 *
 * This class is used to fill the gaps in 2pncMinLocalResidual for the 2PBioMin flow.
 */
template<class TypeTag>
class TwoPThermalMinLocalResidual: public TwoPNCMinLocalResidual<TypeTag>
{
protected:
    typedef TwoPThermalMinLocalResidual<TypeTag> ThisType;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(LocalResidual)) Implementation;
    typedef TwoPNCMinLocalResidual<TypeTag> ParentType;
    typedef BoxLocalResidual<TypeTag> BaseClassType;

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Problem)) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(GridView)) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Scalar)) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(SolutionVector)) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(ElementSolutionVector)) ElementSolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(PrimaryVariables)) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(BoundaryTypes)) BoundaryTypes;

    typedef CompositionalFluidState<Scalar, FluidSystem> FluidState;

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Indices)) Indices;

    enum
    {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,

        numEq = GET_PROP_VALUE(TypeTag, PTAG(NumEq)),
        numPhases = GET_PROP_VALUE(TypeTag, PTAG(NumPhases)),
        numSPhases = GET_PROP_VALUE(TypeTag, PTAG(NumSPhases)),
        numComponents = GET_PROP_VALUE(TypeTag, PTAG(NumComponents)),

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,

        conti0EqIdx = Indices::conti0EqIdx,

        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        plSg = TwoPNCFormulation::plSg,
        pgSl = TwoPNCFormulation::pgSl,
        formulation = GET_PROP_VALUE(TypeTag, Formulation)
    };

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename FVElementGeometry::SubControlVolumeFace SCVFace;


    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
//    typedef Dune::FieldVector<CoordScalar, numComponents> CompVector;
//    typedef Dune::FieldMatrix<CoordScalar, numPhases, numComponents> PhaseCompMatrix;
    typedef Dune::FieldVector<Scalar, dimWorld> DimVector;

//    static constexpr Scalar ImplicitMobilityUpwindWeight = GET_PROP_VALUE(TypeTag, PTAG(ImplicitMobilityUpwindWeight));

public:
    /*!
     * \brief Constructor. Sets the upwind weight.
     */
    TwoPThermalMinLocalResidual()
    {
        // retrieve the upwind weight for the mass conservation equations. Use the value
        // specified via the property system as default, and overwrite
        // it by the run-time parameter from the Dune::ParameterTree
        massUpwindWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);
    };

    /*!
     * \copydoc 2pncMin::computeFlux
     */
    void computeFlux(PrimaryVariables &flux, const int faceIdx, bool onBoundary=false) const
    {
        FluxVariables fluxVars;
        fluxVars.update(this->problem_(),
                      this->element_(),
                      this->fvGeometry_(),
                      faceIdx,
                      this->curVolVars_(),
                      onBoundary);


        flux = 0;
        asImp_()->computeAdvectiveFlux(flux, fluxVars);
        asImp_()->computeDiffusiveFlux(flux, fluxVars);
        Valgrind::CheckDefined(flux);
    }
    /*!
     * \copydoc 2pncMin::computeAdvectiveFlux
     */
    void computeAdvectiveFlux(PrimaryVariables &flux, const FluxVariables &fluxVars) const
    {
        ////////
        // advective fluxes of all components in all phases
        ////////
// 	flux[energyEqIdx] = 0;
        for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
         // data attached to upstream and the downstream vertices
         // of the current phase
         const VolumeVariables &up = this->curVolVars_(fluxVars.upstreamIdx(phaseIdx));
         const VolumeVariables &dn = this->curVolVars_(fluxVars.downstreamIdx(phaseIdx));

         for (unsigned int compIdx = 0; compIdx < numComponents; ++compIdx)
         {
            // add advective flux of current component in current
            // phase
            // if alpha > 0 und alpha < 1 then both upstream and downstream
            // nodes need their contribution
            // if alpha == 1 (which is mostly the case) then, the downstream
            // node is not evaluated
            int eqIdx = conti0EqIdx + compIdx;
            flux[eqIdx] += fluxVars.KmvpNormal(phaseIdx)
                    * ( massUpwindWeight_
                        * up.mobility(phaseIdx)
                        * up.molarDensity(phaseIdx)
                        * up.moleFraction(phaseIdx, compIdx)
                      +
                       (1.0 - massUpwindWeight_)
                        * dn.mobility(phaseIdx)
                        * dn.molarDensity(phaseIdx)
                        * dn.moleFraction(phaseIdx, compIdx));

// 	    flux[energyEqIdx] += fluxVars.volumeFlux(phaseIdx)
// 				  * (massUpwindWeight_
// 				  * (up.fluidState().density(phaseIdx)
// 				  * up.fluidState().enthalpy(phaseIdx))
// 				  +
// 				  (1-massUpwindWeight_)
// 				  * (dn.fluidState().density(phaseIdx)
// 				  * dn.fluidState().enthalpy(phaseIdx)) );

         }
      }
    }

    /*!
     * \copydoc 2pncMin::computeDiffusiveFlux
     *
     * \ for the 2pBioMin model, the diffusive flux is replaced by a dispersive flux
     */
    void computeDiffusiveFlux(PrimaryVariables &flux, const FluxVariables &fluxVars) const
    {
            //Loop calculates the diffusive flux for every component in a phase. The amount of moles of a component
            //(eg CO2 in liquid) in a phase
            //which is not the main component (eg. H2O in the liquid phase) moved from i to j equals the amount of moles moved
            //from the main component in a phase (eg. H2O in the liquid phase) from j to i. So two fluxes in each component loop
            // are calculated in the same phase.

            //here the diffusive flux is replaced by a dispersive flux
        for(int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            for(int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
                if(compIdx != phaseIdx)
                {
                    Scalar diffCont;
                    int eqIdx = compIdx;
                    switch(phaseIdx)
                    {
                    case wPhaseIdx:
//
                        diffCont = - fluxVars.molarDensity(wPhaseIdx)*
                            (fluxVars.normalDisp(wPhaseIdx, compIdx)*//(normalDisp_[phaseIdx][compIdx]*
                            fluxVars.moleFractionGrad(wPhaseIdx, compIdx));
//                          - normalDisp(phaseIdx, compIdx)*
//                            - fluxVars.porousDiffCoeff(compIdx) *
//                            fluxVars.molarDensityAtIP(wPhaseIdx) *
//                            (fluxVars.molarConcGrad(wPhaseIdx, compIdx) * fluxVars.face().normal);
                            flux[eqIdx] += diffCont; // old !!! now mole Fractions are the primary variables!!  * FluidSystem::molarMass(compIdx);
                            flux[wPhaseIdx] -= diffCont; // old !!! now mole Fractions are the primary variables!! * FluidSystem::molarMass(wCompIdx);
//                             flux[temperatureIdx] += fluxVars.normalMatrixHeatFlux();
//                      }
                        break;

                    case nPhaseIdx:
                        if(compIdx == wCompIdx) //|| O2Idx) //only the diffusion coefficient of O2 and H2O in gas is considered
                        {
                            diffCont = - fluxVars.molarDensity(nPhaseIdx)*
                                    (fluxVars.normalDisp(nPhaseIdx, compIdx)*//(normalDisp_[phaseIdx][compIdx]*
                                    fluxVars.moleFractionGrad(nPhaseIdx, compIdx));
//                                -normalDisp(phaseIdx, compIdx)*  //assume the same diffusion coefficient for O2 as for H2O
//                            - fluxVars.porousDiffCoeff(compIdx) *
//                                fluxVars.molarDensityAtIP(nPhaseIdx) *
//                                (fluxVars.molarConcGrad(nPhaseIdx, compIdx) * fluxVars.face().normal);
                                flux[eqIdx] += diffCont; // old !!! now mole Fractions are the primary variables!! * FluidSystem::molarMass(compIdx); // only H2O and no salt in gPhase
                                flux[nPhaseIdx] -= diffCont; // old !!! now mole Fractions are the primary variables!! * FluidSystem::molarMass(nCompIdx);
//                                 flux[temperatureIdx] += fluxVars.normalMatrixHeatFlux();
                        }
                        break;

                    default:DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx); break;
                    }

                }
            }
        }

//              //original diffusive Flux:
//        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
//          for (int compIdx = 0; compIdx < numComponents; ++compIdx)
//          {
////                std::cout   << "PhaseIdx "<< phaseIdx << "CompIdx "<< compIdx    << "dispersion = "  << fluxVars.Dispersion(phaseIdx, compIdx) <<std::endl;
//              Scalar diffCont = - fluxVars.porousDiffCoeff(phaseIdx ,compIdx)* fluxVars.molarDensity(phaseIdx);
//              diffCont *= (fluxVars.moleFractionGrad(phaseIdx, compIdx) * fluxVars.face().normal);
//              flux[conti0EqIdx + compIdx] += diffCont;
//              flux[conti0EqIdx + phaseIdx] -= diffCont;
//          }

    }

    /*!
     * \copydoc 2pncMin::computeSource
     *
     * \additionally the sources due to reactions are calculated
     */
    void computeSource(PrimaryVariables &source, int localVertexIdx)
   {

     const ElementVolumeVariables &elemVolVars = this->curVolVars_();
     const VolumeVariables &volVars = elemVolVars[localVertexIdx];

        PrimaryVariables externalSource;
        PrimaryVariables reactionSource;

        this->problem_().source(externalSource,
                this->element_(),
                this->fvGeometry_(),
                localVertexIdx);

     int dofIdxGlobal = this->problem_().model().vertexMapper().subIndex(this->element_(), localVertexIdx, dim);
     Scalar absgradpw = this->problem_().model().absgradpw(dofIdxGlobal);

     this->problem_().reactionSource(reactionSource,
                this->element_(),
                this->fvGeometry_(),
                localVertexIdx,
                volVars,
                absgradpw); //,fluxVars);

        source = externalSource + reactionSource;

      Valgrind::CheckDefined(source);
   }
    /*!
     * computeDecReactionSource is used only for the decoupled2pbiominmodel.hh
     * \brief Calculate the reactive source terms of the equations
     * \This is a quick and dirty way to decouple the reactions from transport as this is done only when the transport update succeeded.
     *
     * \param source The source/sink in the SCV for each component
     * \param localVertexIdx The index of the SCV
     * \param element The element
     * \param fvGeometry The Geometry of the Finite Volume Element
     * \param volVars The VolumeVariables of the SCV
     * \param mindDt determines whether the reactive sources are resticted to the timestepsize or not
     */
    void computeDecReactionSource(PrimaryVariables &source, Element &element, FVElementGeometry &fvGeometry, int localVertexIdx, VolumeVariables &volVars, bool mindDt)
    {
        Scalar absgradpw = 1000;
        if(mindDt==true)
        {
            FluxVariables fluxVars(this->problem_(),
                    this->element_(),
                    this->fvGeometry_(),
                    localVertexIdx,
                    this->curVolVars_());

            absgradpw = fluxVars.absgradp(wPhaseIdx);
        }
     this->problem_().decReactionSource(source,
                element,
                fvGeometry,
                localVertexIdx,
                volVars,
                absgradpw,
                mindDt); //,fluxVars);


      Valgrind::CheckDefined(source);
   }
    /*!
     * \copydoc 2pncMin::evalOutflowSegment
     */
    void evalOutflowSegment(const IntersectionIterator &isIt,
                            const int scvIdx,
                            const int boundaryFaceIdx)
    {
        const BoundaryTypes &bcTypes = this->bcTypes_(scvIdx);

        // deal with outflow boundaries
        if (bcTypes.hasOutflow())
        {
            PrimaryVariables values(0.0);
//            asImp_()->computeFlux(values, boundaryFaceIdx, true);
            asImp_().computeFlux(values, boundaryFaceIdx, true);
            Valgrind::CheckDefined(values);

            for (int eqIdx = 0; eqIdx < numEq; ++eqIdx)
            {
                if (!bcTypes.isOutflow(eqIdx) )
                    continue;
                this->residual_[scvIdx][eqIdx] += values[eqIdx];
            }
        }
    }

protected:
    Implementation *asImp_()
    { return static_cast<Implementation *> (this); }
    const Implementation *asImp_() const
    { return static_cast<const Implementation *> (this); }

private:
   Scalar massUpwindWeight_;
};

} // end namespace

#endif
