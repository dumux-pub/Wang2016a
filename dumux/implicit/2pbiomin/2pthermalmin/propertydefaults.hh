// -**- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup TwoPThermalMinModel
 * \file
 *
 * \brief Defines default values for most properties required by the
 *        two-phase enzymemineralization fully implicit model.
 */
#ifndef DUMUX_2PENZYMEMIN_PROPERTY_DEFAULTS_HH
#define DUMUX_2PENZYMEMIN_PROPERTY_DEFAULTS_HH

#include "dumux/porousmediumflow/2pncmin/implicit/indices.hh"
#include "model.hh"
#include "dumux/implicit/2pbiomin/2pthermalmin/fluxvariables.hh"
#include "volumevariables.hh"
#include "properties.hh"

#include <dumux/porousmediumflow/2pnc/implicit/newtoncontroller.hh>
#include <dumux/porousmediumflow/implicit/darcyfluxvariables.hh>
#include <dumux/material/spatialparams/implicit.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>

namespace Dumux
{

template <class TypeTag> class TwoPThermalMinModel;
template <class TypeTag> class TwoPThermalMinVolumeVariables;
//template <class TypeTag> class TwoPThermalMinLocalResidual;

namespace Properties {
//////////////////////////////////////////////////////////////////
// Property values
//////////////////////////////////////////////////////////////////

/*!
 * \brief Set the property for the number of secondary components.
 * Secondary components are components calculated from
 * primary components by equilibrium relations and
 * do not have mass balance equation on their own.
 * These components are important in the context of enzyme-mineralization applications.
 * We just forward the number from the fluid system
 *
 */
SET_PROP(TwoPThermalMin, NumSecComponents)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numSecComponents;

};
/*!
 * \brief Set the property for the number of solid phases, excluding the non-reactive matrix.
 *
 * We just forward the number from the fluid system
 *
 */
SET_PROP(TwoPThermalMin, NumSPhases)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numSPhases;
};

/*!
 * \brief Set the property for the number of equations.
 * For each component and each precipitated mineral/solid phase one equation has to
 * be solved.
 */
SET_PROP(TwoPThermalMin, NumEq)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numComponents + FluidSystem::numSPhases;
};

/*!
 * \brief The fluid state which is used by the volume variables to
 *        store the thermodynamic state. This should be chosen
 *        appropriately for the model ((non-)isothermal, equilibrium, ...).
 *        This can be done in the problem.
 */
SET_PROP(TwoPThermalMin, FluidState){
    private:
        typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
        typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    public:
        typedef Dumux::CompositionalFluidState<Scalar, FluidSystem> type;
};

//! Use the 2penzymemin local residual operator
SET_TYPE_PROP(TwoPThermalMin,
              LocalResidual,
              TwoPThermalMinLocalResidual<TypeTag>);
//              TwoPBioMinLocalResidual<TypeTag>);

//! the Model property
SET_TYPE_PROP(TwoPThermalMin, Model, TwoPThermalMinModel<TypeTag>);

//! the VolumeVariables property
SET_TYPE_PROP(TwoPThermalMin, VolumeVariables, TwoPThermalMinVolumeVariables<TypeTag>);

//! the FluxVariables property
SET_TYPE_PROP(TwoPThermalMin, FluxVariables, TwoPThermalMinFluxVariables<TypeTag>);
// SET_TYPE_PROP(TwoPThermalMin, FluxVariables, TwoPBioMinFluxVariables<TypeTag>);

//! The indices required by the isothermal 2pThermalMin model
SET_TYPE_PROP(TwoPThermalMin, Indices, TwoPNCMinIndices <TypeTag, /*PVOffset=*/0>);

//! disable useSalinity for the calculation of osmotic pressure by default
SET_BOOL_PROP(TwoPThermalMin, useSalinity, false);


////! default value for the forchheimer coefficient
//// Source: Ward, J.C. 1964 Turbulent flow in porous media. ASCE J. Hydraul. Div 90.
////        Actually the Forchheimer coefficient is also a function of the dimensions of the
////        porous medium. Taking it as a constant is only a first approximation
////        (Nield, Bejan, Convection in porous media, 2006, p. 10)
//SET_SCALAR_PROP(TwoPNCMin, SpatialParamsForchCoeff, 0.0);

////! Set the CO2 tables used.
//SET_TYPE_PROP(ThermalMinProblem, CO2Tables, Dumux::ThermalMin::CO2Tables);
//
////! Set the problem chemistry
//SET_PROP(ThermalMinProblem, Chemistry)
//{
////    typedef Dumux::ThermalMin::CO2Tables CO2Tables;
//    typedef typename GET_PROP_TYPE(TypeTag, CO2Tables) CO2Tables;
//    typedef Dumux::ThermalCarbonicAcid<TypeTag, CO2Tables> type;
//};

//! Somerton is used as default model to compute the effective thermal heat conductivity
SET_PROP(TwoPThermalMinNI, ThermalConductivityModel)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
public:
    typedef ThermalConductivitySomerton<Scalar, Indices> type;
//    typedef ThermalConductivityAverage<Scalar> type;
};

//////////////////////////////////////////////////////////////////
// Property values for isothermal model required for the general non-isothermal model
//////////////////////////////////////////////////////////////////

// set isothermal Model
SET_TYPE_PROP(TwoPThermalMinNI, IsothermalModel, TwoPThermalMinModel<TypeTag>);

// set isothermal FluxVariables
SET_TYPE_PROP(TwoPThermalMinNI, IsothermalFluxVariables, TwoPThermalMinFluxVariables<TypeTag>);
// SET_TYPE_PROP(TwoPThermalMinNI, IsothermalFluxVariables, TwoPBioMinFluxVariables<TypeTag>);

//set isothermal VolumeVariables
SET_TYPE_PROP(TwoPThermalMinNI, IsothermalVolumeVariables, TwoPThermalMinVolumeVariables<TypeTag>);

//set isothermal LocalResidual
SET_TYPE_PROP(TwoPThermalMinNI, IsothermalLocalResidual, TwoPThermalMinLocalResidual<TypeTag>);
//SET_TYPE_PROP(TwoPThermalMinNI, IsothermalLocalResidual, TwoPBioMinLocalResidual<TypeTag>);

//set isothermal Indices
SET_TYPE_PROP(TwoPThermalMinNI, IsothermalIndices, TwoPNCMinIndices<TypeTag, /*PVOffset=*/0>);

//set isothermal NumEq
SET_PROP(TwoPThermalMinNI, IsothermalNumEq)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numComponents + FluidSystem::numSPhases; // +1 is added by default
};

}
}

#endif
