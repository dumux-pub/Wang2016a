// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Represents all relevant thermodynamic quantities of a
 *        multi-phase, multi-component fluid system assuming
 *        thermodynamic equilibrium.
 */
#ifndef DUMUX_COMPOSITIONAL_SEC_COMP_FLUID_STATE_HH
#define DUMUX_COMPOSITIONAL_SEC_COMP_FLUID_STATE_HH

#include <algorithm>

#include <dune/common/exceptions.hh>
#include <dumux/material/fluidstates/compositional.hh>
#include <dumux/common/valgrind.hh>

namespace Dumux
{
/*!
 * \brief Represents all relevant thermodynamic quantities of a
 *        multi-phase, multi-component fluid system assuming
 *        thermodynamic equilibrium.
 */
template <class Scalar, class FluidSystem>
class CompositionalSecCompFluidState : public CompositionalFluidState<Scalar, FluidSystem>
{
    typedef CompositionalFluidState<Scalar, FluidSystem> ParentType;
public:
    enum { numPhases = FluidSystem::numPhases };
    enum { numComponents = FluidSystem::numComponents };
    enum { numSecComponents = FluidSystem::numSecComponents };

    CompositionalSecCompFluidState()
    {
        // set the composition to 0
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)  {
            for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                moleFraction_[phaseIdx][compIdx] = 0;

            averageMolarMass_[phaseIdx] = 0;
            sumMoleFractions_[phaseIdx] = 0;
        }

        // make everything undefined so that valgrind will complain
        Valgrind::SetUndefined(*this);
    }

    template <class FluidState>
    CompositionalSecCompFluidState(FluidState &fs)
    { assign(fs); }

    /*****************************************************
     * Generic access to fluid properties (No assumptions
     * on thermodynamic equilibrium required)
     *****************************************************/
    /*!
     * \copydoc CompositionalFluidState::saturation
     */
    Scalar saturation(int phaseIdx) const
    { return saturation_[phaseIdx]; }

    /*!
     * \copydoc CompositionalFluidState::moleFraction
     *
     */
    Scalar moleFraction(int phaseIdx, int compIdx) const
    {   if(compIdx<numComponents)
            return moleFraction_[phaseIdx][compIdx];
        else
            return
                moleFractionSecComp_[phaseIdx][compIdx-numComponents];
    }

    /*!
     * \copydoc CompositionalFluidState::massFraction
     */
    Scalar massFraction(int phaseIdx, int compIdx) const
    {
        if(compIdx<numComponents)
            {
            return
            std::abs(sumMoleFractions_[phaseIdx])
            * moleFraction_[phaseIdx][compIdx]
            * FluidSystem::molarMass(compIdx)
            / std::max(1e-40, std::abs(averageMolarMass_[phaseIdx]));
            }
        else
            {
            return
                std::abs(sumMoleFractions_[phaseIdx])
                * moleFractionSecComp_[phaseIdx][compIdx-numComponents]
                * FluidSystem::molarMass(compIdx)
                / std::max(1e-40, std::abs(averageMolarMass_[phaseIdx]));
            }
    }

    /*!
     * \copydoc CompositionalFluidState::averageMolarMass
     */
    Scalar averageMolarMass(int phaseIdx) const
    { return averageMolarMass_[phaseIdx]; }

    /*!
     *\copydoc CompositionalFluidState::molarity
     */
    Scalar molarity(int phaseIdx, int compIdx) const
    { return molarDensity(phaseIdx)*moleFraction(phaseIdx, compIdx); }

    /*!
     * \copydoc CompositionalFluidState::fugacity
     */
    Scalar fugacity(int phaseIdx, int compIdx) const
    { return fugacityCoefficient(phaseIdx, compIdx)*moleFraction(phaseIdx, compIdx)*pressure(phaseIdx); }

    /*!
     * \copydoc CompositionalFluidState::fugacityCoefficient
     */
    Scalar fugacityCoefficient(int phaseIdx, int compIdx) const
    { return fugacityCoefficient_[phaseIdx][compIdx]; }

    /*!
     * \copydoc CompositionalFluidState::molarVolume
     */
    Scalar molarVolume(int phaseIdx) const
    { return 1/molarDensity(phaseIdx); }

    /*!
     * \copydoc CompositionalFluidState::density
     */
    Scalar density(int phaseIdx) const
    { return density_[phaseIdx]; }

    /*!
     * \copydoc CompositionalFluidState::molarDensity
     */
    Scalar molarDensity(int phaseIdx) const
    { return density_[phaseIdx]/averageMolarMass(phaseIdx); }

    /*!
     * \copydoc CompositionalFluidState::temperature
     */
    Scalar temperature(int phaseIdx) const
    { return temperature_; }

    /*!
     * \copydoc CompositionalFluidState::pressure
     */
    Scalar pressure(int phaseIdx) const
    { return pressure_[phaseIdx]; }

    /*!
     * \copydoc CompositionalFluidState::enthalpy
     */
    Scalar enthalpy(int phaseIdx) const
    { return enthalpy_[phaseIdx]; }

    /*!
     * \copydoc CompositionalFluidState::internalEnergy
     */
    Scalar internalEnergy(int phaseIdx) const
    { return enthalpy_[phaseIdx] - pressure(phaseIdx)/density(phaseIdx); }

    /*!
     * \copydoc CompositionalFluidState::viscosity
     */
    Scalar viscosity(int phaseIdx) const
    { return viscosity_[phaseIdx]; }

    /*****************************************************
     * Access to fluid properties which only make sense
     * if assuming thermodynamic equilibrium
     *****************************************************/

    /*!
     * \copydoc CompositionalFluidState::temperature
     */
    Scalar temperature() const
    { return temperature_; }

    /*!
     * \copydoc CompositionalFluidState::fugacity
     */
    Scalar fugacity(int compIdx) const
    { return fugacity(0, compIdx); }


    /*****************************************************
     * Setter methods. Note that these are not part of the
     * generic FluidState interface but specific for each
     * implementation...
     *****************************************************/

    /*!
     * \brief Retrieve all parameters from an arbitrary fluid
     *        state.
     *
     * \note If the other fluid state object is inconsistent with the
     *       thermodynamic equilibrium, the result of this method is
     *       undefined.
     */
    template <class FluidState>
    void assign(const FluidState &fs)
    {
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx) {
            averageMolarMass_[phaseIdx] = 0;
            sumMoleFractions_[phaseIdx] = 0;
            for (int compIdx = 0; compIdx < numComponents; ++compIdx) {
                moleFraction_[phaseIdx][compIdx] = fs.moleFraction(phaseIdx, compIdx);
                fugacityCoefficient_[phaseIdx][compIdx] = fs.fugacityCoefficient(phaseIdx, compIdx);
                averageMolarMass_[phaseIdx] += moleFraction_[phaseIdx][compIdx]*FluidSystem::molarMass(compIdx);
                sumMoleFractions_[phaseIdx] += moleFraction_[phaseIdx][compIdx];
            }
            for (int compIdx = 0; compIdx < numSecComponents; ++compIdx) {
                moleFractionSecComp_[phaseIdx][compIdx] = fs.moleFraction(phaseIdx, compIdx + numComponents);
            }
            averageMolarMass_[phaseIdx] = fs.averageMolarMass(phaseIdx);
            pressure_[phaseIdx] = fs.pressure(phaseIdx);
            saturation_[phaseIdx] = fs.saturation(phaseIdx);
            density_[phaseIdx] = fs.density(phaseIdx);
            enthalpy_[phaseIdx] = fs.enthalpy(phaseIdx);
            viscosity_[phaseIdx] = fs.viscosity(phaseIdx);
        }
        temperature_ = fs.temperature(0);
//        salinity_ = fs.salinity();
    }

    /*!
     * \copydoc CompositionalFluidState::setTemperature
     */
    void setTemperature(Scalar value)
    { temperature_ = value; }

    /*!
     * \copydoc CompositionalFluidState::setTemperature
     */
    void setTemperature(const int phaseIdx, const Scalar value)
    {
        DUNE_THROW(Dune::NotImplemented, "This is a fluidstate for equilibrium, temperature in all phases is assumed to be equal.");
    }

    /*!
     * \copydoc CompositionalFluidState::setPressure
     */
    void setPressure(int phaseIdx, Scalar value)
    { pressure_[phaseIdx] = value; }

    /*!
     * \copydoc CompositionalFluidState::setSaturation
     */
    void setSaturation(int phaseIdx, Scalar value)
    { saturation_[phaseIdx] = value; }

    /*!
     * \copydoc CompositionalFluidState::setMoleFraction
     */
    void setMoleFraction(int phaseIdx, int compIdx, Scalar value)
    {
        Valgrind::CheckDefined(value);
        Valgrind::SetDefined(sumMoleFractions_[phaseIdx]);
        Valgrind::SetDefined(averageMolarMass_[phaseIdx]);
        Valgrind::SetDefined(moleFraction_[phaseIdx][compIdx]);

        if (std::isfinite(averageMolarMass_[phaseIdx])) {
            Scalar delta = value - moleFraction_[phaseIdx][compIdx];

            moleFraction_[phaseIdx][compIdx] = value;

            sumMoleFractions_[phaseIdx] += delta;
            averageMolarMass_[phaseIdx] += delta*FluidSystem::molarMass(compIdx);
        }
        else {

                moleFraction_[phaseIdx][compIdx] = value;

            // re-calculate the mean molar mass
            sumMoleFractions_[phaseIdx] = 0.0;
            averageMolarMass_[phaseIdx] = 0.0;
            for (int compJIdx = 0; compJIdx < numComponents; ++compJIdx) {
                sumMoleFractions_[phaseIdx] += moleFraction_[phaseIdx][compJIdx];
                averageMolarMass_[phaseIdx] += moleFraction_[phaseIdx][compJIdx]*FluidSystem::molarMass(compJIdx);
            }
        }
    }
    /*!
     * \brief Set the mole fraction of a secondary component in a phase []
     */
    void setMoleFractionSecComp(int phaseIdx, int compIdx, Scalar value)
    {
        moleFractionSecComp_[phaseIdx][compIdx-numComponents] = value;
    }
    /*!
     * \copydoc CompositionalFluidState::setFugacityCoefficient
     */
    void setFugacityCoefficient(int phaseIdx, int compIdx, Scalar value)
    { fugacityCoefficient_[phaseIdx][compIdx] = value; }

    /*!
     * \copydoc CompositionalFluidState::setDensity
     */
    void setDensity(int phaseIdx, Scalar value)
    { density_[phaseIdx] = value; }

    /*!
     * \copydoc CompositionalFluidState::setEnthalpy
     */
    void setEnthalpy(int phaseIdx, Scalar value)
    { enthalpy_[phaseIdx] = value; }

    /*!
     * \copydoc CompositionalFluidState::setViscosity
     */
    void setViscosity(int phaseIdx, Scalar value)
    { viscosity_[phaseIdx] = value; }


    /*!
     * \copydoc CompositionalFluidState::checkDefined
     */
    void checkDefined() const
    {
#if HAVE_VALGRIND && ! defined NDEBUG
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx) {
            for (int compIdx = 0; compIdx < numComponents; ++compIdx) {
                Valgrind::CheckDefined(moleFraction_[phaseIdx][compIdx]);
                Valgrind::CheckDefined(fugacityCoefficient_[phaseIdx][compIdx]);
            }
            for (int compIdx = 0; compIdx < numSecComponents; ++compIdx) {
                Valgrind::CheckDefined(moleFractionSecComp_[phaseIdx][compIdx]);
            }
            Valgrind::CheckDefined(averageMolarMass_[phaseIdx]);
            Valgrind::CheckDefined(pressure_[phaseIdx]);
            Valgrind::CheckDefined(saturation_[phaseIdx]);
            Valgrind::CheckDefined(density_[phaseIdx]);
            //Valgrind::CheckDefined(enthalpy_[phaseIdx]);
            Valgrind::CheckDefined(viscosity_[phaseIdx]);
        }

        Valgrind::CheckDefined(temperature_);
#endif // HAVE_VALGRIND
    }

protected:
    Scalar moleFraction_[numPhases][numComponents];
    Scalar moleFractionSecComp_[numPhases][numSecComponents];
    Scalar fugacityCoefficient_[numPhases][numComponents];

    Scalar averageMolarMass_[numPhases];
    Scalar sumMoleFractions_[numPhases];
    Scalar pressure_[numPhases];
    Scalar saturation_[numPhases];
    Scalar density_[numPhases];
    Scalar enthalpy_[numPhases];
    Scalar viscosity_[numPhases];
    Scalar temperature_;
};

} // end namespace Dumux

#endif
