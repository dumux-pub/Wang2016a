Summary
=======

Wang2016a contains a fully working version of the DuMuX code used
for his master's thesis at LH2.
The required DUNE modules are listed at the end of this README.

The Results folder contains the data used to generate the plots (in .vtu format)

Bibtex entry:
@MASTERSTHESIS{WangMSC2016,
  author = {Wang, Kai},
  title = {Development of a numerical model for thermal-ureolysis-induced calcite precipitation},  
  school = {University of Stuttgart, Department of Hydromechanics and Modelling
	of Hydrosystems},
  year = {2016},
  institution = {University of Stuttgart, Department of Hydromechanics and Modelling
	of Hydrosystems}
}

Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installWang2016a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Wang2016a/raw/master/installWang2016a.sh)
in this folder.

```bash
mkdir -p Wang2016a && cd Wang2016a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Wang2016a/raw/master/installWang2016a.sh
sh ./installWang2016a.sh
```
For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

  appl/co2/biomin/thermalmin/,

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

  appl/co2/biomin/thermalmin/thermalmin.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.


Used Versions and Software
==========================


