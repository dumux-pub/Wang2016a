add_input_file_links()
dune_symlink_to_source_files(FILES ../injections)
dune_symlink_to_source_files(FILES ../grids)

# compile thermalmin
add_dumux_test(thermalmin thermalmin thermalmin.cc
  echo
  )